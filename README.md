## To start run following commands

- docker compose build
- docker compose up -d
- docker compose exec app composer require laravel/jetstream
- docker compose exec app php artisan jetstream:install livewire --teams
- docker compose exec app php artisan migrate:fresh



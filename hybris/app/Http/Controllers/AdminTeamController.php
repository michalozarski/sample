<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Laravel\Jetstream\Jetstream;

class AdminTeamController extends Controller
{
    /**
     * Create.
     *
     * @return \Illuminate\View\View
     */
    public function adminCreate()
    {
        if (Gate::denies('isAdmin', Auth::user())) {
            abort(403);
        }

        return view('teams.create');
    }

    /**
     * Show the team details screen for admin.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $teamId
     * @return \Illuminate\View\View
     */
    public function adminDetails(Request $request, $teamId)
    {
        $team = Jetstream::newTeamModel()->findOrFail($teamId);

        if (Gate::denies('isAdmin', Auth::user())) {
            abort(403);
        }

        return view('teams.admin-details', [
            'user' => $request->user(),
            'team' => $team,
        ]);
    }

    /**
     * Show logged user's projects list.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\View\View
     */
    public function adminList(Request $request)
    {
        if (Gate::denies('isAdmin', Auth::user())) {
            abort(403);
        }

        return view('teams.admin-list', [
            'user' => $request->user(),
            'teams' => $request->user()->teams()->where('role', 'admin')->get(),
        ]);
    }
}

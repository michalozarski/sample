<?php

namespace App\Http\Livewire;

use Livewire\Component;

class TeamTokenManager extends Component
{
    /**
     * The team instance.
     *
     * @var mixed
     */
    public $team;
    public $name;
    public $ticker;
    public $max_supply;

    public function mount($team)
    {
        $this->team = $team;
        $this->team->load('team_tokens');
        $this->name = $team->teamToken ? $team->teamToken->name : '';
        $this->ticker = $team->teamToken ? $team->teamToken->ticker : '';
        $this->max_supply = $team->teamToken ? $team->teamToken->max_supply : '';
    }

    public function updateTeamToken()
    {
        $validatedData = $this->validate([
            'max_supply' => 'required',
        ]);

        $validatedData['name'] = 'PSA'.$this->team->id;
        $validatedData['ticker'] = 'PSA'.$this->team->id;
        $validatedData['network'] = 'network';
        $validatedData['contract_address'] = 'contract_address';

        $this->team->team_tokens()->create($validatedData);

        session()->flash('message', 'Team Token updated successfully.');

    }

    public function render()
    {
        return view('livewire.team-token-manager');
    }
}

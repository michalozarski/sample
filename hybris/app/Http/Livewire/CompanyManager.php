<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use App\Models\Company;

class CompanyManager extends Component
{
    public $user;
    public $name;
    public $short_name;
    public $company;
    public $company_id;

    protected $rules = [
        'name' => 'required|min:6',
        'short_name' => 'required',
    ];

    public function mount($user, $company = null)
    {
        $this->user = $user;
        if($company){
            $this->company = $company;
            $this->company_id = $company->id;
            $this->name = $company->name;
            $this->short_name = $company->short_name;
        }
    }

    public function saveCompany()
    {
        $validatedData = $this->validate();
        $validatedData['user_id'] = Auth::id();

        if($this->company_id){
            $this->company->update($validatedData);
        }else{
            Company::create($validatedData);
            $this->reset(['name', 'short_name']);
        }

        session()->flash('message', 'Company created successfully.');
    }

    public function render()
    {
        return view('livewire.company-manager');
    }

}

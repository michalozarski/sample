<?php

namespace App\Http\Livewire;

use App\Models\Team;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use App\Models\Company;

class TeamDetailsManager extends Component
{
    public $team;
    public $team_id;
    public $user;
    public $name;
    public $company;

    public function mount(Company $company, Team $team = null)
    {
        $this->user = Auth::user();

        if(null !== $team){
            $this->team = $team;
            $this->team_id = $team->id;
            $this->name = $team->name;
        }
        $this->company = $company;
    }

    public function saveDetails()
    {

        $validatedData = $this->validate([
            'name' => 'required',
        ]);

        $validatedData['user_id'] = $this->user->id;
        $validatedData['company_id'] = $this->company->id;

        if($this->team_id){
            $this->team->update($validatedData);
        }else{
            Team::create($validatedData);
        }

    }

    public function render()
    {
        return view('livewire.team-details-manager');
    }
}

<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Actions\GetUserBalance;

class TeamMemberRecord extends Component
{
    /**
     * The team instance.
     *
     * @var mixed
     */
    public $team;

    /**
     * The user instance.
     *
     * @var mixed
     */
    public $user;

    /**
     * Balance of the user.
     *
     * @var mixed
     */
    public $balance;

    public function getBalance()
    {
        $userBalance = new GetUserBalance();
        $this->balance = $userBalance->getBalance();
    }

    public function render()
    {
        return view('livewire.team-member-record');
    }
}

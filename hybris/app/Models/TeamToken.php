<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeamToken extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'ticker',
        'max_supply',
        'network',
        'contract_address',
    ];

    /**
     * Get the team that owns the token.
     */
    public function team()
    {
        return $this->belongsTo(Team::class);
    }
}

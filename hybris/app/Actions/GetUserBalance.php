<?php

namespace App\Actions;

use App\Models\User;
use Illuminate\Support\Facades\Http;

class GetUserBalance
{
    public function getBalance()
    {
        $response = Http::get('http://psa_express_1:3000/balance/0x9a0Ed5b2082f5e619D26883a82F54D8ED51DfA2D/0x45C996E49FBC9A89a31a1f1Db043Ac6484eCfaE2');
        $data = $response->json();
        return $data['token']['balance'];
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('team_token', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('team_id');
            $table->string('name', 255);
            $table->string('network', 255);
            $table->string('ticker', 10);
            $table->string('contract_address', 255);
            $table->bigInteger('max_supply');
            $table->timestamps();

            $table->foreign('team_id')->references('id')->on('teams')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('team_token');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            // First, drop the existing 'is_admin' column
            $table->dropColumn('is_admin');

            // Then, add the 'type' column as varchar(50)
            $table->string('account_type', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            // First, drop the 'type' column
            $table->dropColumn('account_type');

            // Then, add back the 'is_admin' column as boolean
            $table->boolean('is_admin')->default(false);
        });
    }
};

<?php

use App\Http\Controllers\AdminTeamController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\myTeamController;
use App\Http\Controllers\CompanyController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/projects', [myTeamController::class, 'list'])->name('teams.list');
Route::get('/projects/{team}', [myTeamController::class, 'details'])->name('teams.details');

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
    Route::get('/my-projects', [AdminTeamController::class, 'adminList'])->name('teams.admin-list');
    Route::get('/my-projects/create', [AdminTeamController::class, 'adminCreate'])->name('my-projects.create');
    Route::get('/my-projects/{project}', [AdminTeamController::class, 'adminDetails'])->name('teams.admin-details');
    Route::get('/companies/index', [CompanyController::class, 'index'])->name('companies.index');
    Route::get('/companies/create', [CompanyController::class, 'create'])->name('companies.create');
    Route::get('/companies/{company}', [CompanyController::class, 'show'])->name('companies.show');
    Route::get('/companies/{company}/edit', [CompanyController::class, 'edit'])->name('companies.edit');
});

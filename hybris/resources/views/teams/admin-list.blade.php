<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Projects List') }}
        </h2>
    </x-slot>

    <div>
        <div class="max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">
            @forelse($teams as $team)
                <div><a href="{{ route('teams.admin-details', $team->id) }}">{{$team->name}}</a></div>
            @empty
                <div>{{__('No items found')}}</div>
            @endforelse

            <br/>
            <a href="{{ route('my-projects.create') }}">{{ __('Create new project') }}</a>
        </div>
    </div>
</x-app-layout>

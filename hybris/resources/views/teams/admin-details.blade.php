<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Project Settings') }}
        </h2>
    </x-slot>

    <div>
        <div class="max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">

            @livewire('teams.update-team-name-form', ['team' => $team])

            @livewire('team-token-manager', ['team' => $team])

            @livewire('team-member-list', ['team' => $team])

        </div>
    </div>
</x-app-layout>

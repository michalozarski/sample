<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Company Info') }} <a href="{{ route('companies.index') }}">{{ __('Back to list') }}</a>
        </h2>
    </x-slot>

    <div>
        <div class="max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">

            <div>{{ __('Company details') }}</div>

            <div>{{ $company->name }}</div>

            <x-section-border />

            <div>{{ __('Projects:') }}</div>

            @if($company->teams->count() > 0)
                @foreach($company->teams as $team)
                    <div><a href="{{ route('teams.show', $team->id) }}">{{ $team->name }}</a></div>
                @endforeach
            @else
                <div>{{ __('Currently no teams') }}</div>
            @endif

            <x-section-border />

            @livewire('team-details-manager', ['company' => $company])

            <br/>
            <div><a href="{{ route('companies.edit', $company->id) }}">{{ __('Edit company') }}</a></div>
        </div>

    </div>
</x-app-layout>

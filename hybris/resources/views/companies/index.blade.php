<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Companies List') }}
        </h2>
    </x-slot>

    <div>
        <div class="max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">
            @forelse($companies as $company)
                <div><a href="{{ route('companies.show', $company->id) }}">{{$company->name}}</a></div>
            @empty
                <div>{{__('No items found')}}</div>
            @endforelse

            <br/>
            <div><a href="{{ route('companies.create') }}">{{ __('Create new company') }}</a></div>
        </div>
    </div>
</x-app-layout>

<div>

    @if ($team->team_tokens->isNotEmpty())
        <x-section-border />

        <div class="mt-10 sm:mt-0">
            <x-action-section>
                <x-slot name="title">
                    {{ __('Project emissions') }}
                </x-slot>

                <x-slot name="description">
                    {{ __('ww') }}
                </x-slot>

                <x-slot name="content">
                    <div class="space-y-6">
                        @foreach ($team->team_tokens as $token)
                            <div class="flex items-center justify-between">
                                <div class="flex items-center">
                                    <div class="ml-4">{{ $token->name }}</div>
                                </div>
                                <div class="flex items-center">
                                    <div class="ml-4">{{ $token->network }}</div>
                                </div>
                                <div class="flex items-center">
                                    <div class="ml-4">{{ $token->max_supply }}</div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </x-slot>
            </x-action-section>
        </div>
    @endif

    <x-section-border />

    <x-form-section submit="updateTeamToken">
        <x-slot name="title">
            {{ __('New Stock Emission') }}
        </x-slot>

        <x-slot name="description">
            {{ __('Provide the amount for new emission.') }}
        </x-slot>

        <x-slot name="form">

            <!-- Token Max Supply -->
            <div class="col-span-6 sm:col-span-4">
                <x-label for="max_supply" value="{{ __('Stock amount') }}" />

                <x-input id="max_supply"
                         type="text"
                         class="mt-1 block w-full"
                         wire:model.defer="max_supply"
                         :disabled="! Gate::check('update', $team)" />

                <x-input-error for="max_supply" class="mt-2" />
            </div>
        </x-slot>

        @if (Gate::check('update', $team))
            <x-slot name="actions">
                <x-action-message class="mr-3" on="saved">
                    {{ __('Saved.') }}
                </x-action-message>

                <x-button>
                    {{ __('Save') }}
                </x-button>
            </x-slot>
        @endif
    </x-form-section>
</div>

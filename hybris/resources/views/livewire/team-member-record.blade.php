<tr>
    <td class="px-6 py-4 whitespace-no-wrap">
        <div class="flex items-center">
            <div class="flex-shrink-0 h-10 w-10">
                @if( $user->profile_photo_url )
                    <img class="w-8 h-8 rounded-full object-cover" src="{{ $user->profile_photo_url }}" alt="{{ $user->name }}">
                @else
                    <img class="h-10 w-10 rounded-full"
                         src="https://www.gravatar.com/avatar/?d=mp&f=y" alt="">
                @endif
            </div>
            <div class="ml-4">
                <div class="text-sm leading-5 font-medium text-gray-900">
                    {{ $user->name }}
                </div>
            </div>
        </div>
    </td>
    <td class="px-6 py-4 whitespace-no-wrap">
        <div class="text-sm leading-5 text-gray-900">{{ $user->email }}</div>
    </td>
    <td class="px-6 py-4 whitespace-no-wrap">
        {{ (int) $balance }}
    </td>
    <td class="px-6 py-4 whitespace-no-wrap text-right text-sm leading-5 font-medium">
        <button type="button" wire:click="getBalance" class="text-indigo-600 hover:text-indigo-900">GetBalance</button>
        @if (Gate::check('removeTeamMember', $team))
            <button class="cursor-pointer ml-6 text-sm text-red-500" wire:click="confirmTeamMemberRemoval('{{ $user->id }}')">
                {{ __('Remove') }}
            </button>
        @endif
    </td>
</tr>
